* [x] opis w clientservice
* [x] result substring logow
* [x] result wyszukiwarka po nazwie
* [x] result clientservice
* [x] result zakres dat
* [x] z poziomu klienta przejscie do uslug a zjego uslug do rezultatow
* [x] zamiast clientservice podzial client service
* [x] sortowanie operacja
* [x] sortowanie wynik
* [x] sortowanie client/service
* [x] generowanie tokenu nowego
* [x] client jako combobox w wyszukiwaniu
* [x] usluga jako combobox w wyszukiwaniu
* [x] tworzenie clientservice z widoku clienta
* [x] nazwa klienta i uslugi w tworzeniu clientservice
* [x] usuwanie ról
* [x] stworzenie dllki
* [x] spolszczyć wszystko(większość)
* [x] w rezultacie: kazde pole wypelnionie po wyszukiwaniu
* [x] w rezultatacie podstawowe szukanie po logu errorze itp
* [x] na dole strony usuwanie w zakresie dat
* [x] usuwanie kaskadowe
* [x] usuwanie uzytkownikow
* [x] opis w edycji service jako texarea
* [x] rezulttaty w liscie uslug
* [x] kolejnosc na navbar
* [x] sortowanie po statusie result
* [x] results dluzsze tresci z enterami (displayfor)
* [x] z klienta do clientservice zamiast service
* [x] ~~procedura skladowana/funkcja wysylajaca posta z poziomu sql~~
* [x] spolszyc usuwanie ról
* [x] mapa w dodawaniu klienta
* [x] mapa w szczegolach klienta
* [x] mapa wszystkich klientow
* [ ] spolszczyć nowododane widoki
* [x] obsluga laczenia pracownika z punktem
* [x] dodawanie punktu z poziomu listy klientow/szczegolow klienta
* [x] przy dodawaniu eventu, dodawanie osob i punktu (czyli chyba kolejne many to many)
* [x] Na liście zdarzeń/usług możliwość wybrania zakresu dat i wyświetlenie na mapie co jest do zrobienia – osobno lista, po kliknięciu w punkt na mapie podpowiedź co do zrobienia
* [x] event/create najpierw klient potem punkty klienta
* [x] szczegoly klienta - lista punktow + możliwość wybrania punktu i rozpoczęcia w ten sposób tworzenie eventa, w któym będzie wybrany Klient i wskazany punkt
* [x] lista eventow mapa
* [x] zmiana w punktach:
	* [x] Wyrzucenie tabelki z góry - bo jak dojdzie dużo punktów będzie ona niepotrzebna
	* [ ]
* [x] ~~event musi miec clientid, pointid, employees(wielu moze byc)~~
* [x] przy dodawaniu eventu: zrobic model ktory ma dane eventu i dane o employee, i na podstawie tego dodawac dane do odpowiednich tabel
trzeba zrobic w event/create cos takiego ze, jak nie ma wybranego puntu, to lista osob pusta, a jak sie wybierze punkt to sie uzupelnia lista osob wszystkimi nazwiskami polaczanymi z tym punktem


* [x] Dodać przejście z mapy do eventu i w eventcie dodać możliwość określenia jego statusu - np. wykonany/w trakcie/ zakończony
	- tabela z statusami domyślnie
	ID = 0 - zaplanowany
	ID = 1 - w trakcie
	ID = 2 - zakończony
	ID = 3 - to skomplikowane

* [x] Dodać przeglądanie eventów po statusach
* [x] widoczność w Kliencie niezakończonych eventów - zadań i możliwość nawigowania do nich
* [x] spolszczyć tam gdzie trzeba
* [ ] intuicyjnie można było przejść do informacji

* [x] https://localhost:44319/Point/Details/13 - przejście do Klienta, wyświetlanie nazwy Klienta, zamiast współrzędnych mapa z punktem i możliwość zmiany tego punktu na inny - możliwość zmiany jest w innym miejscu
* [x] https://localhost:44319/Event/Edit/1019 - spolszczyć + opis długi dodać do eventu
* [ ] Przejść po linkach w serwisie i sprawdzić polszczyznę i dostępność linków oraz czy gdzieś nie ma np. ClientID itp rzeczy
* [x] Sprawdzić czy się da doimplementować szukajkę do mapy - czyli np. by można było wpisać Suwałki, Ełk, Augustów i wycentrowało mapę w tych punktach lub oznaczyło daną lokalizację np. Suwałki, Sikorskiego 4
* [x] https://localhost:44319/Employee - przypisanie osoby do punktu - dodać tutaj - usunąć link z hamburger menu

## Jeśli Visual Studio samo nie stworzy pliku appsettings.json to trzeba go stworzyć w folderze z apka webowa i odpowiednio zmodyfikowac nazwę serwera i bazy danych:

```csharp
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=LAPTOP-II5AMA8R;Database=PRAKTYKI;Trusted_Connection=True;MultipleActiveResultSets=true"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*"
}
```
