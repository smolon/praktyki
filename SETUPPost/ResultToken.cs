﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SETUPPost
{
    public class ResultToken
    {
        public Guid Token { get; set; }
        //public DateTime Date { get; set; }
        public bool? Status { get; set; }
        public string Logs { get; set; }
        public string Errors { get; set; }
    }
}
