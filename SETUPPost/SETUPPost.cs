﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Mono.Options;
using Newtonsoft.Json;

namespace SETUPPost
{
    public static class SETUPPost
    {
        /// <summary>
        /// Dodaje rezultat operacji do bazy danych
        /// </summary>
        /// <param name="url">Endpoint URL</param>
        /// <param name="token">Unikalny identyfikator przypisany do tej usługi</param>
        /// <param name="status">Status opreacji</param>
        /// <param name="error">Treść błędu</param>
        /// <param name="log">Treść logu</param>
        /// <returns>Zwraca ID rezultatu. W przypadku niepowodzenia zwraca treść błędu</returns>
        public static string PostMessage(string url, string token, bool? status, string error, string log)
        {
            
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "POST";
                Guid token2 = Guid.Parse(token);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36";
                    ResultToken resultToken = new ResultToken()
                    {
                        Token = token2,
                        Status = status,
                        Errors = error,
                        Logs = log
                    };
                    string json = JsonConvert.SerializeObject(resultToken);
                    streamWriter.Write(json);
                    //Console.WriteLine(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    //Console.WriteLine(result);
                    return result;
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                return (e.Message);
            }
        }
    }
}

