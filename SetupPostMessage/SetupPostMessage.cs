﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SetupPostMessage
{
    //https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2010/ms247123(v=vs.100)?redirectedfrom=MSDN
    public class SetupPostMessage
    {
        //--Create Function Split(@StringToSplit nvarchar(max), @splitOnChars nvarchar(max) )
        //--	returns Table(
        //--			Results nvarchar(max)
        //--	)
        //--	AS
        //--			External name CLRDemo.[SetupPostMessage.SetupPostMessage].Split
        //--	GO

        //select* from dbo.Split('1,2,3#,4,5:6:7#~8~9',',:~')


        //SQL Functions require an additional "SqlFunction" Attribute.
        //This attribute provides SQL server with additional meta data information it needs
        //regarding our custom function. In this example we are not accessing any data, and our
        //function is deterministic. So we let SQL know those facts about our function with
        //the DataAccess and IsDeterministic parameters of our attribute.
        //Additionally, SQL needs to know the name of a function it can defer to when it needs
        //to convert the object we have returned from our function into a structure that SQL
        //can understand. This is provided by the "FillRowMethodName" shown below.
        [SqlFunction(
            DataAccess = DataAccessKind.None,
            FillRowMethodName = "MyFillRowMethod"
            , IsDeterministic = true)
        ]
        //SQL Functions must be declared as Static. Table Valued functions must also
        //return a class that implements the IEnumerable interface. Most built in
        //.NET collections and arrays already implement this interface.
        public static IEnumerable Split(string stringToSplit, string delimiters)
        {
            //One line of C# code splits our string on one or more delimiters...
            //A string array is one of many objects that are returnable from
            //a SQL CLR function - as it implements the required IEnumerable interface.
            string[] elements = stringToSplit.Split(delimiters.ToCharArray());
            return elements;
        }
        public static string PostMessage(string url, string tkn, bool? status, string logs, string errors)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                ((sender, certificate, chain, sslPolicyErrors) => true);
            string json = "";
            try
            {
                Guid token = Guid.Parse(tkn);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = "POST";
                DateTime dateTime = DateTime.Now;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36";
                    ResultToken resultToken = new ResultToken()
                    {
                        Token = token,
                        Date = dateTime,
                        Status = status,
                        Errors = errors,
                        Logs = logs
                    };
                    resultToken.Logs.Replace("\"", "'");
                    //string json = JsonConvert.SerializeObject(resultToken);
                    json = "{\n\t\"Token\":\"[token]\",\n\t\"Status\":[status],\n\t\"Logs\":\"[logs]\",\n\t\"Errors\":\"[errors]\"\n}";

                    json = json.Replace("[token]", resultToken.Token.ToString());
                    if (resultToken.Status.Equals(null)){
                        json = json.Replace("[status]", "null");
                    }
                    else
                    {
                        json = json.Replace("[status]", resultToken.Status.ToString().ToLower());
                    }
                    json = json.Replace("[logs]", resultToken.Logs);
                    json = json.Replace("[errors]", resultToken.Errors);
                    streamWriter.Write(json);
                    Console.WriteLine(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    //Console.WriteLine(result);
                    return result;
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                return e.Message;
            }
        }
        //SQL needs to defer to user code to translate the an IEnumerable item into something
        //SQL Server can understand. In this case we convert our string to a SqlChar object...
        public static void MyFillRowMethod(Object theItem, out SqlChars results)
        {
            results = new SqlChars(theItem.ToString());
        }
    }
}
