﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using Mono.Options;
using System.Collections.Generic;
using System.Linq;

namespace console_praktyki
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            bool? stat = true;
            string error = "";
            string log = "";
            var p = new OptionSet()
            {
                {"s|status=", "status",
                (bool v) => stat = v},
                {"e|error=", "content of errors",
                v => error = v},
                {"l|log=", "log content",
                v => log = v}
            };
            List<string> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Write("console_praktyki: ");
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `greet --help' for more information.");
                return;
            }
            foreach(var item in extra)
            {
                Console.WriteLine(item);
            }

            bool? status = true;
            string errors = "";
            string logs = "";
            Console.WriteLine("status: " + stat.ToString());
            Console.WriteLine("errors: " + error);
            Console.WriteLine("logs: " + log);
            status = stat;
            errors = error;
            logs = log;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings.Get("url"));
                httpWebRequest.Method = "POST";
                Guid token = Guid.Parse(ConfigurationManager.AppSettings.Get("token"));
                DateTime dateTime = DateTime.Now;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36";
                    ResultToken resultToken = new ResultToken()
                    {
                        Token = token,
                        //Date = dateTime,
                        Status = status,
                        Errors = errors,
                        Logs = logs
                    };
                    string json = JsonConvert.SerializeObject(resultToken);
                    streamWriter.Write(json);
                    Console.WriteLine(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Console.WriteLine(result);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
