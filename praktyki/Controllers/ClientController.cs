﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    [Authorize(Roles = "Admin, Mod")]
    public class ClientController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClientController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Client
        //public async Task<IActionResult> Index()
        //{
        //    return View(await _context.Client.ToListAsync());
        //}
        public async Task<IActionResult> Index(string search)
        {
            var clients = from m in _context.Client
                           select m;
            if (!String.IsNullOrEmpty(search))
            {
                clients = clients.Where(s => s.Name.Contains(search));
            }
            return View(await clients.ToListAsync());
        }
        // GET: Client/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            //ClientService clientService = await _context.ClientService
            //    .FirstOrDefaultAsync(c => c.ClientId == id);
            if (id == null)
            {
                return NotFound();
            }
            //var services = await _context.Service
            //    .Include(cs => cs.ClientService)
            //    .Where(r => r.ClientService == clientService)
            //    .ToListAsync();
            var client = await _context.Client.Include(p => p.Point).ThenInclude(e => e.Events).FirstOrDefaultAsync(m => m.ClientId == id);
            var clientService = await _context.ClientService
                .FirstOrDefaultAsync(cs => cs.ClientId == id);
            List<ClientService> clientS = await _context.ClientService
                .Where(i => i.ClientId == id).ToListAsync();
            List<Service> services = new List<Service>();
            List<Point> points = new List<Point>();
            if (client == null)
            {
                return NotFound();
            }
            foreach(var item in clientS)
            {
                services.Add(await _context.Service.FindAsync(item.ServiceId));
            }
            foreach(var item in _context.Point)
            {
                if (item.ClientId == client.ClientId)
                    points.Add(item);
            }
            ViewBag.Services = clientS;
            ViewBag.Points = points;
            return View(client);
        }

        // GET: Client/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ClientId,Name,Latitude,Longitude")] Client client)
        {
            if (ModelState.IsValid)
            {
                _context.Add(client);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(client);
        }

        // GET: Client/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await _context.Client.FindAsync(id);
            if (client == null)
            {
                return NotFound();
            }
            return View(client);
        }

        // POST: Client/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ClientId,Name")] Client client)
        {
            if (id != client.ClientId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(client);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientExists(client.ClientId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(client);
        }

        // GET: Client/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await _context.Client
                .FirstOrDefaultAsync(m => m.ClientId == id);
            if (client == null)
            {
                return NotFound();
            }

            return View(client);
        }

        // POST: Client/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var client = await _context.Client.FindAsync(id);
            _context.Client.Remove(client);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClientExists(int id)
        {
            return _context.Client.Any(e => e.ClientId == id);
        }
    }
}
