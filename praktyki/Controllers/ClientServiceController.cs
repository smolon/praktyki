﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    [Authorize(Roles = "Admin, Mod")]
    public class ClientServiceController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClientServiceController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ClientService
        //public async Task<IActionResult> Index()
        //{
        //    var applicationDbContext = _context.ClientService.Include(c => c.Client).Include(c => c.Service);
        //    return View(await applicationDbContext.ToListAsync());
        //}
        public async Task<IActionResult> Index(string search)
        {
            var clientServices = from m in _context.ClientService
                          select m;
            clientServices = clientServices.Include(c => c.Client).Include(s => s.Service);
            if (!String.IsNullOrEmpty(search))
            {
                clientServices = clientServices.Where(s => s.Client.Name.Contains(search) || s.Service.Name.Contains(search) || s.Token.ToString().Contains(search));
            }

            return View(await clientServices.ToListAsync());
        }
        // GET: ClientService/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientService = await _context.ClientService
                .Include(c => c.Client)
                .Include(c => c.Service)
                .FirstOrDefaultAsync(m => m.ClientServiceId == id);
            if (clientService == null)
            {
                return NotFound();
            }

            return View(clientService);
        }

        // GET: ClientService/Create
        public IActionResult Create(int? id)
        {
            if (id.HasValue)
            {
                ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", _context.Client.Find(id).ClientId);
                ViewData["ServiceId"] = new SelectList(_context.Service, "ServiceId", "Name");
                ViewData["PassedId"] = true;
            }
            else
            {
                ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name");
                ViewData["ServiceId"] = new SelectList(_context.Service, "ServiceId", "Name");
                ViewData["PassedId"] = false;
            }
            return View();
        }

        // POST: ClientService/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ClientServiceId,Token,ClientId,ServiceId,StartDate,Opis")] ClientService clientService)
        {
            if (ModelState.IsValid)
            {
                clientService.Token = Guid.NewGuid();
                clientService.StartDate = DateTime.Now;
                _context.Add(clientService);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", clientService.ClientId);
            ViewData["ServiceId"] = new SelectList(_context.Service, "ServiceId", "Name", clientService.ServiceId);
            return View(clientService);
        }

        // GET: ClientService/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientService = await _context.ClientService.FindAsync(id);
            if (clientService == null)
            {
                return NotFound();
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", clientService.ClientId);
            ViewData["ServiceId"] = new SelectList(_context.Service, "ServiceId", "Name", clientService.ServiceId);
            return View(clientService);
        }

        // POST: ClientService/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ClientServiceId,Token,ClientId,ServiceId,StartDate")] ClientService clientService)
        {
            if (id != clientService.ClientServiceId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(clientService);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientServiceExists(clientService.ClientServiceId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", clientService.ClientId);
            ViewData["ServiceId"] = new SelectList(_context.Service, "ServiceId", "Name", clientService.ServiceId);
            return View(clientService);
        }

        // GET: ClientService/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientService = await _context.ClientService
                .Include(c => c.Client)
                .Include(c => c.Service)
                .FirstOrDefaultAsync(m => m.ClientServiceId == id);
            if (clientService == null)
            {
                return NotFound();
            }

            return View(clientService);
        }

        // POST: ClientService/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var clientService = await _context.ClientService.FindAsync(id);
            _context.ClientService.Remove(clientService);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClientServiceExists(int id)
        {
            return _context.ClientService.Any(e => e.ClientServiceId == id);
        }
    }
}
