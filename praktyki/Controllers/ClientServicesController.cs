﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientServicesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ClientServicesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/ClientServices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientService>>> GetClientService()
        {
            return await _context.ClientService.ToListAsync();
        }

        // GET: api/ClientServices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClientService>> GetClientService(int id)
        {
            var clientService = await _context.ClientService.FindAsync(id);

            if (clientService == null)
            {
                return NotFound();
            }

            return clientService;
        }

        // PUT: api/ClientServices/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClientService(int id, ClientService clientService)
        {
            if (id != clientService.ClientServiceId)
            {
                return BadRequest();
            }

            _context.Entry(clientService).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientServiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ClientServices
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ClientService>> PostClientService(ClientService clientService)
        {
            _context.ClientService.Add(clientService);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClientService", new { id = clientService.ClientServiceId }, clientService);
        }

        // DELETE: api/ClientServices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ClientService>> DeleteClientService(int id)
        {
            var clientService = await _context.ClientService.FindAsync(id);
            if (clientService == null)
            {
                return NotFound();
            }

            _context.ClientService.Remove(clientService);
            await _context.SaveChangesAsync();

            return clientService;
        }

        private bool ClientServiceExists(int id)
        {
            return _context.ClientService.Any(e => e.ClientServiceId == id);
        }
    }
}
