﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    public class EmployeePointController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EmployeePointController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EmployeePoint
        public async Task<IActionResult> Index()
        {
            ViewData["MyTitle"] = "Lista przypisanych osób do punktów";
            var applicationDbContext = _context.EmployeePoint.Include(e => e.Employee).Include(e => e.Point);
            if(applicationDbContext.Any())
                return View(await applicationDbContext.ToListAsync());
            else
            {
                ViewData["MyTitle"] = "Żadna osoba nie została jeszcze przypisana do żadnego z punktów";
                return View();
            }        
        }


        // GET: EmployeePoint/Create
        public IActionResult Create()
        {
            ViewData["EmployeeId"] = new SelectList(_context.Employee, "EmployeeId", "FullName");
            ViewData["PointId"] = new SelectList(_context.Point, "PointId", "Name");
            return View();
        }

        // POST: EmployeePoint/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeId,PointId")] EmployeePoint employeePoint)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employeePoint);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EmployeeId"] = new SelectList(_context.Employee, "EmployeeId", "EmployeeId", employeePoint.EmployeeId);
            ViewData["PointId"] = new SelectList(_context.Point, "PointId", "PointId", employeePoint.PointId);
            return View(employeePoint);
        }

        

        // GET: EmployeePoint/Delete/5
        public async Task<IActionResult> Delete(int? PointId, int? EmployeeId)
        {
            if (PointId == null || EmployeeId == null)
            {
                return NotFound();
            }

            var employeePoint = await _context.EmployeePoint
                .Include(e => e.Employee)
                .Include(e => e.Point)
                .FirstOrDefaultAsync(m => m.EmployeeId == EmployeeId && m.PointId == PointId);
            if (employeePoint == null)
            {
                return NotFound();
            }

            return View(employeePoint);
        }

        // POST: EmployeePoint/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? PointId, int? EmployeeId)
        {
            var employeePoint = await _context.EmployeePoint.FindAsync(EmployeeId, PointId);
            _context.EmployeePoint.Remove(employeePoint);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeePointExists(int? PointId, int? EmployeeId)
        {
            return _context.EmployeePoint.Any(e => e.EmployeeId == EmployeeId && e.PointId == PointId);
        }
    }
}
