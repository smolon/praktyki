﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeePointsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public EmployeePointsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/EmployeePoints
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeePoint>>> GetEmployeePoint()
        {
            return await _context.EmployeePoint.ToListAsync();
        }

        // GET: api/EmployeePoints/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeePoint>> GetEmployeePoint(int id)
        {
            var EmployeePoint = await _context.EmployeePoint.FindAsync(id);

            if (EmployeePoint == null)
            {
                return NotFound();
            }

            return EmployeePoint;
        }

        // PUT: api/EmployeePoints/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutEmployeePoint(int id, EmployeePoint EmployeePoint)
        //{
        //    if (id != EmployeePoint.EmployeePointId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(EmployeePoint).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!EmployeePointExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/EmployeePoints
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPost]
        //public async Task<ActionResult<EmployeePoint>> PostEmployeePoint(EmployeePoint EmployeePoint)
        //{
        //    _context.EmployeePoint.Add(EmployeePoint);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetEmployeePoint", new { id = EmployeePoint.EmployeePointId }, EmployeePoint);
        //}

        //// DELETE: api/EmployeePoints/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<EmployeePoint>> DeleteEmployeePoint(int id)
        //{
        //    var EmployeePoint = await _context.EmployeePoint.FindAsync(id);
        //    if (EmployeePoint == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.EmployeePoint.Remove(EmployeePoint);
        //    await _context.SaveChangesAsync();

        //    return EmployeePoint;
        //}

        //private bool EmployeePointExists(int id)
        //{
        //    return _context.EmployeePoint.Any(e => e.EmployeePointId == id);
        //}
    }
}
