﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    public class EventController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EventController(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Map(string FromDate, string ToDate, int? Status)
        {
            DateTime dt1 = new DateTime();
            DateTime dt2 = new DateTime();
            ViewBag.Status = null;
            try
            {
                dt1 = DateTime.Parse(FromDate);
                dt2 = DateTime.Parse(ToDate);
                ViewData["DateTime1"] = FromDate;
                ViewData["DateTime2"] = ToDate;
            }
            catch (ArgumentNullException)
            {
                dt1 = DateTime.Now.AddDays(-30);
                dt2 = DateTime.Now;
                ViewData["DateTime1"] = XmlConvert.ToString(dt1, (XmlDateTimeSerializationMode)3).Remove(XmlConvert.ToString(dt1, (XmlDateTimeSerializationMode)3).IndexOf("."));
                ViewData["DateTime2"] = XmlConvert.ToString(dt2, (XmlDateTimeSerializationMode)3).Remove(XmlConvert.ToString(dt2, (XmlDateTimeSerializationMode)3).IndexOf("."));

            }
            var applicationDbContext = _context.Event.Include(p => p.Point).ThenInclude(c => c.Client).Where(d => d.Date >= dt1 && d.Date <= dt2);
            if (Status >= 0 && Status < 4)
            {
                applicationDbContext = (IOrderedQueryable<Event>)applicationDbContext.Where(s => s.Status == Status);
                ViewBag.Status = Status;
            }
            return View(await applicationDbContext.ToListAsync());
        }
        // GET: Event
        public async Task<IActionResult> Index()
        {
            return View(await _context.Event.ToListAsync());
        }

        // GET: Event/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event
                .FirstOrDefaultAsync(m => m.EventId == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // GET: Event/Create
        public IActionResult Create(int? clientId, int? pointId)
        {
            if (clientId.HasValue && pointId.HasValue)
            {
                ViewBag.Employees = new MultiSelectList(_context.Employee, "EmployeeId", "FullName");
                ViewBag.Points = new SelectList(_context.Point, "PointId", "Name", _context.Point.Find(pointId).PointId);
                ViewBag.Clients = new SelectList(_context.Client, "ClientId", "Name", _context.Client.Find(clientId).ClientId);
                ViewData["PassedId"] = true;
                ViewBag.PointId = pointId;
            }
            else
            {
                ViewBag.Employees = new MultiSelectList(_context.Employee, "EmployeeId", "FullName");
                ViewBag.Points = new SelectList(_context.Point, "PointId", "Name");
                ViewBag.Clients = new SelectList(_context.Client, "ClientId", "Name");
                ViewData["PassedId"] = false;
                ViewBag.PointId = 1;
            }

            return View();
        }

        // POST: Event/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EventViewModel eventViewModel)
        {
            if (ModelState.IsValid)
            {
                Event @event = new Event()
                {
                    Description = eventViewModel.Description,
                    Date = eventViewModel.Date,
                    Point = _context.Point.Find(eventViewModel.PointId),
                    Status = eventViewModel.Status,
                    DescriptionLong = eventViewModel.DescriptionLong
                };
                _context.Add(@event);
                await _context.SaveChangesAsync();
                if (eventViewModel.EmployeesId != null)
                {
                    foreach (var item in eventViewModel.EmployeesId)
                    {
                        EmployeeEvent employeeEvent = new EmployeeEvent()
                        {
                            EployeeId = item,
                            EventId = @event.EventId
                        };
                        _context.Add(employeeEvent);
                        await _context.SaveChangesAsync();
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(eventViewModel);
        }

        // GET: Event/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }
            return View(@event);
        }

        // POST: Event/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EventId,Description,DescriptionLong,Date,PointId,Status")] Event @event)
        {
            if (id != @event.EventId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Event event2 = _context.Event.Find(id);
                    event2.Status = @event.Status;
                    event2.Date = @event.Date;
                    event2.Description = @event.Description;
                    event2.DescriptionLong = @event.DescriptionLong;
                    _context.Update(event2);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@event.EventId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(@event);
        }

        // GET: Event/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Event
                .FirstOrDefaultAsync(m => m.EventId == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // POST: Event/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @event = await _context.Event.FindAsync(id);
            _context.Event.Remove(@event);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EventExists(int id)
        {
            return _context.Event.Any(e => e.EventId == id);
        }
    }
}
