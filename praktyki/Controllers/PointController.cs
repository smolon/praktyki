﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    public class PointController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PointController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Point
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Point.Include(p => p.Client);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Point/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var point = await _context.Point
                .Include(p => p.Client)
                .FirstOrDefaultAsync(m => m.PointId == id);
            if (point == null)
            {
                return NotFound();
            }

            return View(point);
        }

        // GET: Point/Create
        public IActionResult Create(int? id)
        {
            if (id.HasValue)
            {
                ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", _context.Client.Find(id).ClientId);
                ViewData["PassedId"] = true;
            }
            else
            {
                ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name");
                ViewData["PassedId"] = false;
            }
            return View();
        }

        // POST: Point/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PointId,Name,Description,Latitude,Longitude,ClientId")] Point point)
        {
            if (ModelState.IsValid)
            {
                _context.Add(point);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", point.ClientId);
            return View(point);
        }

        // GET: Point/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var point = await _context.Point.FindAsync(id);
            if (point == null)
            {
                return NotFound();
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", point.ClientId);
            return View(point);
        }

        // POST: Point/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PointId,Name,Description,Latitude,Longitude,ClientId")] Point point)
        {
            if (id != point.PointId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(point);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PointExists(point.PointId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "Name", point.ClientId);
            return View(point);
        }

        // GET: Point/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var point = await _context.Point
                .Include(p => p.Client)
                .FirstOrDefaultAsync(m => m.PointId == id);
            if (point == null)
            {
                return NotFound();
            }

            return View(point);
        }

        // POST: Point/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var point = await _context.Point.FindAsync(id);
            _context.Point.Remove(point);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PointExists(int id)
        {
            return _context.Point.Any(e => e.PointId == id);
        }
    }
}
