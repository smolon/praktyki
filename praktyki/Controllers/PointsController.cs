﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PointsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public PointsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Points
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Point>>> GetPoint()
        {
            return await _context.Point.ToListAsync();
        }

        // GET: api/Points/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Point>> GetPoint(int id)
        {
            var Point = await _context.Point.FindAsync(id);

            if (Point == null)
            {
                return NotFound();
            }

            return Point;
        }

        // PUT: api/Points/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPoint(int id, Point Point)
        {
            if (id != Point.PointId)
            {
                return BadRequest();
            }

            _context.Entry(Point).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PointExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Points
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Point>> PostPoint(Point Point)
        {
            _context.Point.Add(Point);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPoint", new { id = Point.PointId }, Point);
        }

        // DELETE: api/Points/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Point>> DeletePoint(int id)
        {
            var Point = await _context.Point.FindAsync(id);
            if (Point == null)
            {
                return NotFound();
            }

            _context.Point.Remove(Point);
            await _context.SaveChangesAsync();

            return Point;
        }

        private bool PointExists(int id)
        {
            return _context.Point.Any(e => e.PointId == id);
        }
    }
}
