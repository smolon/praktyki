﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;
using System.Xml;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace praktyki.Controllers
{
    [Authorize(Roles = "Admin, Mod")]
    public class ResultController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ResultController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Result
        public async Task<IActionResult> Index(string search, string Client, string Service, string FromDate, string ToDate, string Status, string Error, string Log, string sortOrder, string currentFilter, int? pageNumber, int? elemsOnPage)
        {
            DateTime dt1 = new DateTime();
            DateTime dt2 = new DateTime();
            try
            {
                dt1 = DateTime.Parse(FromDate);
                dt2 = DateTime.Parse(ToDate);
                ViewData["DateTime1"] = FromDate;
                ViewData["DateTime2"] = ToDate;
            }
            catch(ArgumentNullException)
            {
                dt1 = DateTime.Now.AddDays(-30);
                dt2 = DateTime.Now;
                ViewData["DateTime1"] = XmlConvert.ToString(dt1, (XmlDateTimeSerializationMode)3).Remove(XmlConvert.ToString(dt1, (XmlDateTimeSerializationMode)3).IndexOf("."));
                ViewData["DateTime2"] = XmlConvert.ToString(dt2, (XmlDateTimeSerializationMode)3).Remove(XmlConvert.ToString(dt2, (XmlDateTimeSerializationMode)3).IndexOf("."));

            }

            var applicationDbContext = _context.Result
                .Include(r => r.ClientService)
                    .ThenInclude(c => c.Client)
                .Include(r => r.ClientService)
                    .ThenInclude(s => s.Service)
                .Where(d => d.Datetime >= dt1 && d.Datetime <= dt2)
                .OrderByDescending(s => s.ResultId);
            ViewData["CurrentSort"] = sortOrder;
            ViewData["ClientSortParm"] = sortOrder == "client_asc" ? "client_desc" : "client_asc";
            ViewData["ServiceSortParm"] = sortOrder == "service_asc" ? "service_desc" : "service_asc";
            ViewData["ErrorSortParm"] = sortOrder == "error_asc" ? "error_desc" : "error_asc";
            ViewData["LogSortParm"] = sortOrder == "log_asc" ? "log_desc" : "log_asc";
            ViewData["StatusSortParm"] = sortOrder == "status_asc" ? "status_desc" : "status_asc";
            ViewData["DateSortParm"] = String.IsNullOrEmpty(sortOrder) ? "date_asc" : "";
            ViewData["Log"] = "";
            ViewData["Error"] = "";
            ViewData["SelectedClient"] = "";
            ViewData["SelectedService"] = "";
            ViewData["SelectedStatus"] = "";
            ViewData["Search"] = "";
            if (search != null)
            {
                pageNumber = 1;
            }
            else
            {
                search = currentFilter;
            }
            ViewData["CurrentFilter"] = search;

            applicationDbContext = sortOrder switch
            {
                "client_desc" => applicationDbContext.OrderByDescending(s => s.ClientService.Client.Name),
                "client_asc" => applicationDbContext.OrderBy(s => s.ClientService.Client.Name),
                "service_desc" => applicationDbContext.OrderByDescending(s => s.ClientService.Service.Name),
                "service_asc" => applicationDbContext.OrderBy(s => s.ClientService.Service.Name),
                "error_desc" => applicationDbContext.OrderByDescending(s => s.Errors),
                "error_asc" => applicationDbContext.OrderBy(s => s.Errors),
                "log_desc" => applicationDbContext.OrderByDescending(s => s.Logs),
                "log_asc" => applicationDbContext.OrderBy(s => s.Logs),
                "date_asc" => applicationDbContext.OrderBy(s => s.Datetime),
                "status_asc" => applicationDbContext.OrderBy(s => s.Status),
                "status_desc" => applicationDbContext.OrderByDescending(s => s.Status),
                _ => applicationDbContext.OrderByDescending(s => s.Datetime),
            };
            if (!String.IsNullOrEmpty(search))
            {
                applicationDbContext = (IOrderedQueryable<Result>)applicationDbContext.Where(s => s.ClientService.Service.Name.Contains(search) || 
                                        s.ClientService.Client.Name.Contains(search) || 
                                        s.Errors.Contains(search) || 
                                        s.Logs.Contains(search));
                ViewData["Search"] = search;
            }
            if (!String.IsNullOrEmpty(Client))
            {
                applicationDbContext = (IOrderedQueryable<Result>)applicationDbContext.Where(c => c.ClientService.Client.Name.Contains(Client));
                ViewData["SelectedClient"] = Client;
            }
            if (!String.IsNullOrEmpty(Service))
            {
                applicationDbContext = (IOrderedQueryable<Result>)applicationDbContext.Where(c => c.ClientService.Service.Name.Contains(Service));
                ViewData["SelectedService"] = Service;
            }
            if (!String.IsNullOrEmpty(Status) && Status != "Null")
            {
                applicationDbContext = (IOrderedQueryable<Result>)applicationDbContext.Where(c => c.Status == Convert.ToBoolean(Status));
                ViewData["SelectedStatus"] = Status;
            }
            else if (!String.IsNullOrEmpty(Status) && Status == "Null")
            {
                applicationDbContext = (IOrderedQueryable<Result>)applicationDbContext.Where(c => c.Status == null);
            }
            if (!String.IsNullOrEmpty(Error))
            {
                applicationDbContext = (IOrderedQueryable<Result>)applicationDbContext.Where(c => c.Errors.Contains(Error));
                ViewData["Error"] = Error;
            }
            if (!String.IsNullOrEmpty(Log))
            {
                applicationDbContext = (IOrderedQueryable<Result>)applicationDbContext.Where(c => c.Logs.Contains(Log));
                ViewData["Log"] = Log;
            }
            int pageSize;
            if (elemsOnPage.HasValue)
            {
                pageSize = (int)elemsOnPage;
                Set("pageSize", pageSize.ToString());
                ViewData["active"] = pageSize.ToString();
            }
            else
            {
                if (Request.Cookies["pageSize"] == null)
                {
                    Set("pageSize", "20");
                    ViewData["active"] = "20";
                    pageSize = 20;
                }
                else
                {
                    pageSize = int.Parse(Request.Cookies["pageSize"]);
                    ViewData["active"] = pageSize.ToString();
                }
            }
            
            //return View(await applicationDbContext.ToListAsync());

            List<Client> clients = _context.Client.ToList();
            List<Service> services = _context.Service.ToList();
            ViewData["services"] = services;
            ViewData["clients"] = clients;
            return View(await PaginatedList<Result>.CreateAsync(applicationDbContext.AsNoTracking(), pageNumber ?? 1, pageSize));
        }
        public void Set(string key, string value)
        {

            CookieOptions option = new CookieOptions
            {
                Secure = true,
                Expires = DateTime.Now.AddYears(20),
                SameSite = SameSiteMode.None
            };
            Response.Cookies.Append(key, value, option);
            
        }
        public IActionResult ClearForm()
        {
            return RedirectToAction("Index", new { search = "", client = "", service = "", FromDate = "", ToDate = "", Status = "", Error = "", Log = "", sortOrder = "", currentFilter = "" });
        }
        [HttpPost]
        public IActionResult DeleteInDateRange(string fromDate, string toDate)
        {
            var applicationDbContext = _context.Result
                .Where(d => d.Datetime >= DateTime.Parse(fromDate) && d.Datetime <= DateTime.Parse(toDate));
            _context.Result.RemoveRange(applicationDbContext);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
        // GET: Result/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var result = await _context.Result
                .Include(r => r.ClientService)
                    .ThenInclude(c => c.Client)
                .Include(r => r.ClientService)
                    .ThenInclude(s => s.Service)
                .FirstOrDefaultAsync(m => m.ResultId == id);
            if (result == null)
            {
                return NotFound();
            }

            return View(result);
        }

        // GET: Result/Create
        public IActionResult Create()
        {
            ViewData["ClientServiceId"] = new SelectList(_context.ClientService, "ClientServiceId", "ClientServiceId");
            return View();
        }

        // POST: Result/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ResultId,Datetime,Status,Logs,Errors,ClientServiceId")] Result result)
        {
            if (ModelState.IsValid)
            {
                _context.Add(result);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientServiceId"] = new SelectList(_context.ClientService, "ClientServiceId", "ClientServiceId", result.ClientServiceId);
            return View(result);
        }

        // GET: Result/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var result = await _context.Result.FindAsync(id);
        //    if (result == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["ClientServiceId"] = new SelectList(_context.ClientService, "ClientServiceId", "ClientServiceId", result.ClientServiceId);
        //    return View(result);
        //}

        //// POST: Result/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("ResultId,Datetime,Status,Logs,Errors,ClientServiceId")] Result result)
        //{
        //    if (id != result.ResultId)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(result);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!ResultExists(result.ResultId))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["ClientServiceId"] = new SelectList(_context.ClientService, "ClientServiceId", "ClientServiceId", result.ClientServiceId);
        //    return View(result);
        //}

        //// GET: Result/Delete/5
        //[Authorize(Roles = "Admin")]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var result = await _context.Result
        //        .Include(r => r.ClientService)
        //        .FirstOrDefaultAsync(m => m.ResultId == id);
        //    if (result == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(result);
        //}

        //// POST: Result/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var result = await _context.Result.FindAsync(id);
        //    _context.Result.Remove(result);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        private bool ResultExists(int id)
        {
            return _context.Result.Any(e => e.ResultId == id);
        }
    }
}
