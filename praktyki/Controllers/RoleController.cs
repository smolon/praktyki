﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using praktyki.Data;
using praktyki.Models;

namespace praktyki.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<IdentityUser> _userManager;
        //private readonly Manager
        private readonly ApplicationDbContext _context;
        public RoleController(RoleManager<IdentityRole> roleManager, ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _roleManager = roleManager;
            _context = context;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            var roles = _roleManager.Roles.ToList();
            return View(roles);
        }
        public IActionResult ListUsers()
        {
            var users = _userManager.Users.ToList();
            return View(users);
        }
        public async Task<IActionResult> Update(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            List<IdentityUser> members = new List<IdentityUser>();
            List<IdentityUser> nonMembers = new List<IdentityUser>();
            foreach (IdentityUser user in _userManager.Users)
            {
                var list = await _userManager.IsInRoleAsync(user, role.Name) ? members : nonMembers;
                list.Add(user);
            }
            return View(new RoleEdit
            {
                Role = role,
                Members = members,
                NonMembers = nonMembers
            });
        }
        [HttpPost]
        public async Task<IActionResult> Update(RoleModification model)
        {
            IdentityResult result;
            if (ModelState.IsValid)
            {
                foreach (string userId in model.AddIds ?? new string[] { })
                {
                    IdentityUser user = await _userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        result = await _userManager.AddToRoleAsync(user, model.RoleName);
                        if (!result.Succeeded)
                            RedirectToAction(nameof(Update));
                    }
                }
                foreach (string userId in model.DeleteIds ?? new string[] { })
                {
                    IdentityUser user = await _userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        result = await _userManager.RemoveFromRoleAsync(user, model.RoleName);
                        if (!result.Succeeded)
                            RedirectToAction(nameof(Update));
                    }
                }
            }

            if (ModelState.IsValid)
                return RedirectToAction(nameof(Index));
            else
                return await Update(model.RoleId);
        }
        public async Task<IActionResult> EditUserRole(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var roles = await _userManager.GetRolesAsync(user);
            UserRoles ur = new UserRoles
            {
                UserId = user.Id,
                UserName = user.UserName,
                RoleNames = (List<string>)roles
            };
            ViewBag.RoleNames = new SelectList(_roleManager.Roles.ToList(), "Id", "Name");
            //ViewData["roles"] = roles;
            return View(ur);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteUserRole(string id, string role)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                await _userManager.RemoveFromRolesAsync(user, (IEnumerable<string>)role.ToList());
                return RedirectToAction(nameof(ListUsers));
            }
            catch (Exception e)
            {
                return View("Error");
            }
            
        }
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            return View(user);
        }
        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                await _userManager.DeleteAsync(user);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception e)
            {
                return View("Error");
            }
            
        }
        public IActionResult Create()
        {
            return View(new IdentityRole());
        }
        //public async Task<IActionResult> DeleteFromUser()
        //{
        //    var userRoles = await _context.UserRoles.ToListAsync();
        //}
        [HttpPost]
        public async Task<IActionResult> Create (IdentityRole role)
        {
            await _roleManager.CreateAsync(role);
            return RedirectToAction("Index");
        }
        //public async Task<IActionResult> Delete()
        //{
        //    AspNetUserRoles anur = new AspNetUserRoles();
        //    anur = _context.UserRoles.Include(u => u.)
        //    return View(anur);
        //}
        // GET: Role/Assign
        public async Task<ActionResult> Manage()
        {
            var adminRole = await _roleManager.FindByNameAsync("Admin");
            var assignableRoles = _roleManager.Roles.ToList();
            //linijka nizej sprawia ze nie mozna przypisac nikogo do admina
            //assignableRoles.RemoveAt(assignableRoles.IndexOf(adminRole));
            ViewData["Name"] = new SelectList(assignableRoles, "Name", "Name");
            ViewData["UserName"] = new SelectList(_userManager.Users, "UserName", "UserName");
            return View(new RoleModel());
        }

        // POST: Role/Assign
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(RoleModel roleModel)
        {
            if (ModelState.IsValid)
            {
                //Blokowanie przypisania komus admina
                //if (roleModel.Name == "Admin")
                //{
                //    ViewData["Message"] = "Invalid Request.";
                //    return View("Info");
                //}
                var user = await _userManager.FindByEmailAsync(roleModel.UserName);
                if (user != null)
                {
                    if (await _roleManager.RoleExistsAsync(roleModel.Name))
                    {
                        if (await _userManager.IsInRoleAsync(user, roleModel.Name))
                        {
                            ViewData["Message"] = $@"Użytkownik {roleModel.UserName} już posiada rolę {roleModel.Name}.";
                            return View("Info");
                        }
                        else
                        {
                            await _userManager.AddToRoleAsync(user, roleModel.Name);
                            ViewData["Message"] = $@"Użytkownik {roleModel.UserName} został przypisany do roli {roleModel.Name}.";
                            return View("Info");
                        }
                    }
                    else
                    {
                        ViewData["Message"] = "Invalid Request.";
                        return View("Info");
                    }
                }
                else
                {
                    ViewData["Message"] = "Invalid Request.";
                    return View("Info");
                }
            }
            return View(roleModel);
        }
    }
}
