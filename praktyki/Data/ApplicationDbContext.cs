﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using praktyki.Models;

namespace praktyki.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<EmployeePoint>()
                .HasKey(t => new { t.EmployeeId, t.PointId });
            builder.Entity<EmployeePoint>()
                .HasOne(pt => pt.Employee)
                .WithMany(p => p.EmployeePoints)
                .HasForeignKey(pt => pt.EmployeeId);
            builder.Entity<EmployeePoint>()
                .HasOne(pt => pt.Point)
                .WithMany(p => p.EmployeePoints)
                .HasForeignKey(pt => pt.PointId);
            builder.Entity<EmployeeEvent>()
                .HasKey(t => new { t.EployeeId, t.EventId });
            builder.Entity<EmployeeEvent>()
                .HasOne(ee => ee.Employee)
                .WithMany(e => e.EmployeeEvents)
                .HasForeignKey(f => f.EployeeId);
            builder.Entity<EmployeeEvent>()
                .HasOne(ee => ee.Event)
                .WithMany(e => e.EmployeeEvents)
                .HasForeignKey(f => f.EventId);
        }
        public DbSet<praktyki.Models.Service> Service { get; set; }
        public DbSet<praktyki.Models.Client> Client { get; set; }
        public DbSet<praktyki.Models.Result> Result { get; set; }
        public DbSet<praktyki.Models.ClientService> ClientService { get; set; }
        public DbSet<praktyki.Models.Point> Point { get; set; }
        public DbSet<praktyki.Models.Employee> Employee { get; set; }
        public DbSet<praktyki.Models.Event> Event { get; set; }
        public DbSet<praktyki.Models.EmployeePoint> EmployeePoint { get; set; }
        public DbSet<praktyki.Models.EmployeeEvent> EmployeeEvent { get; set; }
    }
}
