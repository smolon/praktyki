﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace praktyki.Migrations
{
    public partial class bla : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "client",
            //    columns: table => new
            //    {
            //        client_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_client", x => x.client_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "service",
            //    columns: table => new
            //    {
            //        service_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(maxLength: 50, nullable: true),
            //        description = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_service", x => x.service_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "client_service",
            //    columns: table => new
            //    {
            //        client_service_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        token = table.Column<Guid>(nullable: false),
            //        client_id = table.Column<int>(nullable: false),
            //        service_id = table.Column<int>(nullable: false),
            //        start_date = table.Column<DateTime>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_client_service", x => x.client_service_id);
            //        table.ForeignKey(
            //            name: "FK_client_service_client_client_id",
            //            column: x => x.client_id,
            //            principalTable: "client",
            //            principalColumn: "client_id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_client_service_service_service_id",
            //            column: x => x.service_id,
            //            principalTable: "service",
            //            principalColumn: "service_id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "result",
            //    columns: table => new
            //    {
            //        result_id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        datetime = table.Column<DateTime>(nullable: false),
            //        status = table.Column<bool>(nullable: true),
            //        logs = table.Column<string>(nullable: true),
            //        errors = table.Column<string>(nullable: true),
            //        client_service_id = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_result", x => x.result_id);
            //        table.ForeignKey(
            //            name: "FK_result_client_service_client_service_id",
            //            column: x => x.client_service_id,
            //            principalTable: "client_service",
            //            principalColumn: "client_service_id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_client_service_client_id",
            //    table: "client_service",
            //    column: "client_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_client_service_service_id",
            //    table: "client_service",
            //    column: "service_id");

            //migrationBuilder.CreateIndex(
            //    name: "IX_result_client_service_id",
            //    table: "result",
            //    column: "client_service_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "result");

            //migrationBuilder.DropTable(
            //    name: "client_service");

            //migrationBuilder.DropTable(
            //    name: "client");

            //migrationBuilder.DropTable(
            //    name: "service");
        }
    }
}
