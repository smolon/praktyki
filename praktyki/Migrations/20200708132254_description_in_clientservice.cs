﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace praktyki.Migrations
{
    public partial class description_in_clientservice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "description",
                table: "client_service",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "description",
                table: "client_service");
        }
    }
}
