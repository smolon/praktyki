﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace praktyki.Migrations
{
    public partial class latlong : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "latitude",
                table: "client",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "longitude",
                table: "client",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "latitude",
                table: "client");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "client");
        }
    }
}
