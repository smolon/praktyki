﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace praktyki.Migrations
{
    public partial class clean : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EventId",
                table: "employee",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_employee_EventId",
                table: "employee",
                column: "EventId");

            migrationBuilder.AddForeignKey(
                name: "FK_employee_event_EventId",
                table: "employee",
                column: "EventId",
                principalTable: "event",
                principalColumn: "event_id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
