﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace praktyki.Migrations
{
    public partial class fluentemevent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
            
           
            migrationBuilder.CreateTable(
                name: "EmployeeEvent",
                columns: table => new
                {
                    EployeeId = table.Column<int>(nullable: false),
                    EventId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeEvent", x => new { x.EployeeId, x.EventId });
                    table.ForeignKey(
                        name: "FK_EmployeeEvent_employee_EployeeId",
                        column: x => x.EployeeId,
                        principalTable: "employee",
                        principalColumn: "employee_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeEvent_event_EventId",
                        column: x => x.EventId,
                        principalTable: "event",
                        principalColumn: "event_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeEvent_EventId",
                table: "EmployeeEvent",
                column: "EventId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeEvent");

            //migrationBuilder.AddColumn<int>(
            //    name: "EventId",
            //    table: "employee",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.CreateIndex(
            //    name: "IX_employee_EventId",
            //    table: "employee",
            //    column: "EventId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_employee_event_EventId",
            //    table: "employee",
            //    column: "EventId",
            //    principalTable: "event",
            //    principalColumn: "event_id",
            //    onDelete: ReferentialAction.Cascade);
        }
    }
}
