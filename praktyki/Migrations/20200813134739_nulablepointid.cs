﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace praktyki.Migrations
{
    public partial class nulablepointid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_event_point_PointId",
                table: "event");

            migrationBuilder.AlterColumn<int>(
                name: "PointId",
                table: "event",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_event_point_PointId",
                table: "event",
                column: "PointId",
                principalTable: "point",
                principalColumn: "point_id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_event_point_PointId",
                table: "event");

            migrationBuilder.AlterColumn<int>(
                name: "PointId",
                table: "event",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_event_point_PointId",
                table: "event",
                column: "PointId",
                principalTable: "point",
                principalColumn: "point_id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
