﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace praktyki.Migrations
{
    public partial class requiredpointid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_event_point_PointId",
                table: "event");

            migrationBuilder.AlterColumn<int>(
                name: "PointId",
                table: "event",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_event_point_PointId",
                table: "event",
                column: "PointId",
                principalTable: "point",
                principalColumn: "point_id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_event_point_PointId",
                table: "event");

            migrationBuilder.AlterColumn<int>(
                name: "PointId",
                table: "event",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_event_point_PointId",
                table: "event",
                column: "PointId",
                principalTable: "point",
                principalColumn: "point_id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
