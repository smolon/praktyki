﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace praktyki.Models
{
    [Table("client")]
    public partial class Client
    {
        public Client()
        {
            ClientService = new HashSet<ClientService>();
        }

        [Key]
        [Column("client_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientId { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }
        [Column("latitude")]
        [Display(Name="Szerokość geograficzna")]
        public string Latitude { get; set; }
        [Column("longitude")]
        [Display(Name = "Długość geograficzna")]
        public string Longitude { get; set; }
        [InverseProperty("Client")]
        public virtual ICollection<ClientService> ClientService { get; set; }
        [InverseProperty("Client")]
        public virtual ICollection<Point> Point { get; set; }
    }
}
