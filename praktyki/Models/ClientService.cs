﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace praktyki.Models
{
    [Table("client_service")]
    public partial class ClientService
    {
        public ClientService()
        {
            Result = new HashSet<Result>();
        }

        [Key]
        [Column("client_service_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientServiceId { get; set; }
        [Column("token")]
        public Guid Token { get; set; }
        [Column("client_id")]
        public int ClientId { get; set; }
        [Column("service_id")]
        public int ServiceId { get; set; }
        [Column("start_date")]
        [Display(Name = "Data rozpoczęcia")]
        public DateTime StartDate { get; set; }
        [Column("description")]
        [Display(Name = "Opis")]
        public string Opis { get; set; }
        [ForeignKey(nameof(ClientId))]
        [InverseProperty("ClientService")]
        [Display(Name = "Klient")]
        public virtual Client Client { get; set; }
        [ForeignKey(nameof(ServiceId))]
        [InverseProperty("ClientService")]
        [Display(Name = "Usługa")]
        public virtual Service Service { get; set; }
        [InverseProperty("ClientService")]
        public virtual ICollection<Result> Result { get; set; }
        public string GuidString
        {
            get
            {
                return Guid.NewGuid().ToString();
            }
        }
    }
}
