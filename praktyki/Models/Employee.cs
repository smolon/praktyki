﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace praktyki.Models
{
    [Table("employee")]
    public partial class Employee
    {
        [Key]
        [Column("employee_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeId { get; set; }

        [Column("first_name")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }
        [Column("last_name")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }
        [Column("phone")]
        [Display(Name = "Numer telefonu")]
        public string? Phone { get; set; }
        [Column("email")]
        [Display(Name = "Email")]
        public string? Email { get; set; }
        [Column("description")]
        [Display(Name = "Opis")]
        public string Description { get; set; }
        public ICollection<EmployeePoint> EmployeePoints { get; set; }
        public ICollection<EmployeeEvent> EmployeeEvents { get; set; }
        [NotMapped]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
