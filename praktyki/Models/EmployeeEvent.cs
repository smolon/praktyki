﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praktyki.Models
{
    public class EmployeeEvent
    {
        public int EployeeId { get; set; }
        public Employee Employee { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }
    }
}
