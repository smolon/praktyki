﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace praktyki.Models
{
    [Table("employee_point")]
    public partial class EmployeePoint
    {
        [Key]
        [Column(Order = 1)]
        [ForeignKey("Employee")]
        [Display(Name ="Osoba")]
        public int EmployeeId { get; set; }
        [Key]
        [Column(Order = 2)]
        [ForeignKey("Point")]
        [Display(Name = "Punkt")]
        public int PointId { get; set; }
        [Display(Name = "Punkt")]
        public Point Point { get; set; }
        [Display(Name = "Osoba")]
        public Employee Employee { get; set; }
    }
}
