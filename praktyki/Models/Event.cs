﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace praktyki.Models
{
    [Table("event")]
    public partial class Event
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("event_id")]
        [Display(Name = "Zdarzenie")]
        public int EventId { get; set; }
        [Required]
        [Column("description")]
        [Display(Name = "Opis")]
        public string Description { get; set;}
        [Required]
        [Column("description_long")]
        [Display(Name = "Szczegółowy opis")]
        public string DescriptionLong { get; set; }
        [Column("date")]
        [Display(Name = "Data")]
        public DateTime Date { get; set; }
        [Column("status")]
        [Range(0,3)]
        public int Status { get; set; }
        [Required]
        public int PointId { get; set; }
        [Display(Name = "Punkt")]
        public virtual Point Point { get; set; }
        public ICollection<EmployeeEvent> EmployeeEvents { get; set; }
    }
}
