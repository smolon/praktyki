﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace praktyki.Models
{
    public class EventViewModel
    {
        [Display(Name = "Opis")]
        public string Description { get; set; }
        [Display(Name = "Data")]
        public DateTime Date { get; set; }
        public Client Client { get; set; }
        [Display(Name ="Szczegółowy opis")]
        public string DescriptionLong { get; set; }
        public int PointId { get; set; }
        public ICollection<int> EmployeesId { get; set; }
        public int Status { get; set; }
    }
}
