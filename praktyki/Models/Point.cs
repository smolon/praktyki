﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace praktyki.Models
{
    [Table("point")]
    public partial class Point
    {
        [Key]
        [Column("point_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Punkt")]
        public int PointId { get; set; }
        [Column("name")]
        [Display(Name = "Nazwa")]
        [Required]
        public string Name { get; set; }
        [Column("description")]
        [Display(Name = "Opis")]
        [Required]
        public string Description { get; set; }
        [Column("latitude")]
        [Required]
        [Display(Name = "Szerokość geograficzna")]
        public string Latitude { get; set; }
        [Column("longitude")]
        [Required]
        [Display(Name = "Długość geograficzna")]
        public string Longitude { get; set; }
        [Column("client_id")]
        [Display(Name = "Klient")]
        public int ClientId { get; set; }
        [ForeignKey(nameof(ClientId))]
        [InverseProperty("Point")]
        [Display(Name = "Klient")]
        public virtual Client Client { get; set; }
        public ICollection<EmployeePoint> EmployeePoints { get; set; }
        public ICollection<Event> Events { get; set; }
    }
}
