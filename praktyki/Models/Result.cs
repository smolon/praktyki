﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace praktyki.Models
{
    
    [Table("result")]
    public partial class Result
    {
        [Key]
        [Column("result_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ResultId { get; set; }
        [Column("datetime")]
        [Display(Name = "Data")]
        public DateTime Datetime { get; set; }
        [Column("status")]
        public bool? Status { get; set; }
        [Column("logs")]
        [Display(Name = "Logi")]
        public string Logs { get; set; }
        [Column("errors")]
        [Display(Name = "Błędy")]
        public string Errors { get; set; }
        [Column("client_service_id")]
        public int ClientServiceId { get; set; }

        [ForeignKey(nameof(ClientServiceId))]
        [InverseProperty("Result")]
        public virtual ClientService ClientService { get; set; }
    }
}
