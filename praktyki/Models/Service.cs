﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace praktyki.Models
{
    [Table("service")]
    public partial class Service
    {
        public Service()
        {
            ClientService = new HashSet<ClientService>();
        }

        [Key]
        [Column("service_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ServiceId { get; set; }
        [Column("name")]
        [StringLength(50)]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }
        [Column("description")]
        [Display(Name = "Opis")]
        public string Description { get; set; }

        [InverseProperty("Service")]
        public virtual ICollection<ClientService> ClientService { get; set; }
    }
}
