﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace praktyki.Services
{
    public class EmailSender : IEmailSender
    {
        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public AuthMessageSenderOptions Options { get; } //set only via Secret Manager

        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Execute();
        }
        public async Task Execute()
        {
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("a.smolonek1@wp.pl", "admin");
            var subject = "zobaczymy czy to zadziała";
            var to = new EmailAddress("a.smolonek1@wp.pl", "user");
            var plainTextContent = "zwykly tekst";
            var htmlContent = "<strong>a tutaj html heheheh</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
            System.Diagnostics.Debug.WriteLine(response.ToString());
        }
        //public Task Execute(string apiKey, string subject, string message, string email)
        //{
        //    var client = new SendGridClient(apiKey);
        //    var msg = new SendGridMessage()
        //    {
        //        From = new EmailAddress("Joe@contoso.com", Options.SendGridUser),
        //        Subject = subject,
        //        PlainTextContent = message,
        //        HtmlContent = message
        //    };
        //    msg.AddTo(new EmailAddress(email));

        //    // Disable click tracking.
        //    // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
        //    msg.SetClickTracking(false, false);

        //    return client.SendEmailAsync(msg);
        //}
    }
}