USE [master]
GO
/****** Object:  Database [PRAKTYKI]    Script Date: 30.08.2020 17:00:12 ******/
CREATE DATABASE [PRAKTYKI]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PRAKTYKI', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\PRAKTYKI.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PRAKTYKI_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\PRAKTYKI_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [PRAKTYKI] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PRAKTYKI].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PRAKTYKI] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PRAKTYKI] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PRAKTYKI] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PRAKTYKI] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PRAKTYKI] SET ARITHABORT OFF 
GO
ALTER DATABASE [PRAKTYKI] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PRAKTYKI] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PRAKTYKI] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PRAKTYKI] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PRAKTYKI] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PRAKTYKI] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PRAKTYKI] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PRAKTYKI] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PRAKTYKI] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PRAKTYKI] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PRAKTYKI] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PRAKTYKI] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PRAKTYKI] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PRAKTYKI] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PRAKTYKI] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PRAKTYKI] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PRAKTYKI] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PRAKTYKI] SET RECOVERY FULL 
GO
ALTER DATABASE [PRAKTYKI] SET  MULTI_USER 
GO
ALTER DATABASE [PRAKTYKI] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PRAKTYKI] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PRAKTYKI] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PRAKTYKI] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PRAKTYKI] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PRAKTYKI', N'ON'
GO
ALTER DATABASE [PRAKTYKI] SET QUERY_STORE = OFF
GO
USE [PRAKTYKI]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[client]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[client](
	[client_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[latitude] [nvarchar](max) NULL,
	[longitude] [nvarchar](max) NULL,
 CONSTRAINT [PK_client] PRIMARY KEY CLUSTERED 
(
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[client_service]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[client_service](
	[client_service_id] [int] IDENTITY(1,1) NOT NULL,
	[token] [uniqueidentifier] NOT NULL,
	[client_id] [int] NOT NULL,
	[service_id] [int] NOT NULL,
	[start_date] [datetime2](7) NOT NULL,
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_client_service_1] PRIMARY KEY CLUSTERED 
(
	[client_service_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employee]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee](
	[employee_id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](max) NULL,
	[last_name] [nvarchar](max) NULL,
	[phone] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_employee] PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employee_point]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee_point](
	[EmployeeId] [int] NOT NULL,
	[PointId] [int] NOT NULL,
 CONSTRAINT [PK_EmployeePoint] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC,
	[PointId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeEvent]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeEvent](
	[EployeeId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
 CONSTRAINT [PK_EmployeeEvent] PRIMARY KEY CLUSTERED 
(
	[EployeeId] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[event]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[event](
	[event_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[date] [datetime2](7) NOT NULL,
	[PointId] [int] NOT NULL,
	[status] [int] NOT NULL,
	[description_long] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_event] PRIMARY KEY CLUSTERED 
(
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[point]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[point](
	[point_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[latitude] [nvarchar](max) NOT NULL,
	[longitude] [nvarchar](max) NOT NULL,
	[client_id] [int] NOT NULL,
 CONSTRAINT [PK_point] PRIMARY KEY CLUSTERED 
(
	[point_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[result]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[result](
	[result_id] [int] IDENTITY(1,1) NOT NULL,
	[datetime] [datetime2](7) NOT NULL,
	[status] [bit] NULL,
	[logs] [varchar](max) NULL,
	[errors] [varchar](max) NULL,
	[client_service_id] [int] NOT NULL,
 CONSTRAINT [PK_result] PRIMARY KEY CLUSTERED 
(
	[result_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[service]    Script Date: 30.08.2020 17:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[service](
	[service_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_service] PRIMARY KEY CLUSTERED 
(
	[service_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706074209_bla', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706074622_test', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706074757_autoincrements', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200708132254_description_in_clientservice', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200709082350_identity', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200730084733_latlong', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200804085554_testpoint', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200805095602_testmany', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200805105739_pointedit', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200810103607_requiredinpoint', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813122631_clean', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813122820_clean2', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813123748_employeeeventfluent', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813124149_employeeeventfluent2', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813130424_fluentemevent', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813132214_pointid_event', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813134739_nulablepointid', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813134945_requiredpointid', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200813135039_requireddesc', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200826063515_status_in_event', N'3.1.5')
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200830101626_longdescription', N'3.1.5')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'3866a641-ceeb-4af5-bbaa-c3f62c358967', N'Mod', N'MOD', N'986b1abd-e953-477d-8164-5390c2bcfb70')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'48ec1520-1483-43cb-8900-b709f5f7fe9c', N'User', N'USER', N'73ea68bb-4dde-42a4-90cf-137524778e6d')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'7374e069-5acb-44ad-8076-24aa9272fc1a', N'Admin', N'ADMIN', N'091252e6-a993-4c13-8931-5383e7eb08b6')
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Microsoft', N'695fdfd14732fa8b', N'Microsoft', N'd451e355-997d-4b0d-909d-0f252058d47d')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9637aaa9-3360-49d6-8688-8bb325548e2e', N'3866a641-ceeb-4af5-bbaa-c3f62c358967')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'211c5b29-a3c2-428d-9499-77e2c39625bd', N'48ec1520-1483-43cb-8900-b709f5f7fe9c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'28389e45-8747-4462-a896-93e5312ad8a5', N'48ec1520-1483-43cb-8900-b709f5f7fe9c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'69b03250-9f47-4620-a917-7510c29e99fd', N'48ec1520-1483-43cb-8900-b709f5f7fe9c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9637aaa9-3360-49d6-8688-8bb325548e2e', N'48ec1520-1483-43cb-8900-b709f5f7fe9c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f4b7d1e4-480c-4f8d-a304-3386e84bab5d', N'48ec1520-1483-43cb-8900-b709f5f7fe9c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'28389e45-8747-4462-a896-93e5312ad8a5', N'7374e069-5acb-44ad-8076-24aa9272fc1a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9637aaa9-3360-49d6-8688-8bb325548e2e', N'7374e069-5acb-44ad-8076-24aa9272fc1a')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'211c5b29-a3c2-428d-9499-77e2c39625bd', N'blabla@a.com', N'BLABLA@A.COM', N'blabla@a.com', N'BLABLA@A.COM', 1, N'AQAAAAEAACcQAAAAEMXvU9gbhQKNwURSrVWWnfXl8O6+eHTBfq1pmCJAfaNaQ9/DjOY2DR0wD4bE9wtLDw==', N'O5FS2M645QSJLJMWXWFHERIJUOYYRH5B', N'b61089de-0ecc-4510-bc0d-5b8b3e23c472', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'28389e45-8747-4462-a896-93e5312ad8a5', N'szymon.liszewski@setup.suwalki.pl', N'SZYMON.LISZEWSKI@SETUP.SUWALKI.PL', N'szymon.liszewski@setup.suwalki.pl', N'SZYMON.LISZEWSKI@SETUP.SUWALKI.PL', 1, N'AQAAAAEAACcQAAAAEMJeEtmam8TdsDJPTpaP+CikCGtRlK0FUGKs62EBJYsDjzw7jS8atEUA9iBFX/1oHw==', N'HWRGLZ5KS5ZZB4YRRCHGR7JNGB4DDPPJ', N'b8977984-006c-4058-8534-59a06f772c73', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'69b03250-9f47-4620-a917-7510c29e99fd', N'fakeaccount_long_email@wp.pl', N'FAKEACCOUNT_LONG_EMAIL@WP.PL', N'fakeaccount_long_email@wp.pl', N'FAKEACCOUNT_LONG_EMAIL@WP.PL', 1, N'AQAAAAEAACcQAAAAECApRXuxWC8wnV7e+939Eew+O7PXJcyc330Os40LSZmJDGvOBxkUbS1ky1HaFmerPQ==', N'HEMMUURYRV4DRNPAR7EHZEJD3R6NOLL4', N'4b8e87a6-99e8-4499-8a2d-62a205f45230', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'9637aaa9-3360-49d6-8688-8bb325548e2e', N'asd@ad.com', N'ASD@AD.COM', N'asd@ad.com', N'ASD@AD.COM', 1, N'AQAAAAEAACcQAAAAEBU4mPyBBpWGUen4iJ/Aw3LuxeKNxLAeLEvO2SAm8UDowqyZ409P4WiG2O8H4a25Eg==', N'RY2EPT6HIE6FYKZEFXNI6FOBZ2QPE2VJ', N'a13a7dda-6c50-4df5-b3f1-691f6a27bc8e', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'd451e355-997d-4b0d-909d-0f252058d47d', N'smolonek@gmail.com', N'SMOLONEK@GMAIL.COM', N'smolonek@gmail.com', N'SMOLONEK@GMAIL.COM', 0, NULL, N'KMGT7JWLNMYWBDV7NDHXYOTS6SSUYHAG', N'bb12f1a8-c0f8-4254-932e-7828ad0583d3', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'f4b7d1e4-480c-4f8d-a304-3386e84bab5d', N'bla@bl.com', N'BLA@BL.COM', N'bla@bl.com', N'BLA@BL.COM', 1, N'AQAAAAEAACcQAAAAEDKJyv/HAezn9q0kuEjuM82PMVGjZd9E+vK1uKbyhlrRACGDdykfeouxTW62jPMahg==', N'VVNATMOXK75MLAXWCBKFLNTTOIGG7TEZ', N'820795bf-d459-4c0c-9435-481565dce90b', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUserTokens] ([UserId], [LoginProvider], [Name], [Value]) VALUES (N'9637aaa9-3360-49d6-8688-8bb325548e2e', N'[AspNetUserStore]', N'AuthenticatorKey', N'P3WWFL3THU3BHX7ZW7RP3322XH35MV45')
GO
SET IDENTITY_INSERT [dbo].[client] ON 
GO
INSERT [dbo].[client] ([client_id], [name], [latitude], [longitude]) VALUES (1, N'Klient A', NULL, NULL)
GO
INSERT [dbo].[client] ([client_id], [name], [latitude], [longitude]) VALUES (3, N'bardzo dluga nazwa klienta', N'53.306241', N' 21.64883')
GO
INSERT [dbo].[client] ([client_id], [name], [latitude], [longitude]) VALUES (4, N'mapa', N'53.306245', N' 23.64893')
GO
INSERT [dbo].[client] ([client_id], [name], [latitude], [longitude]) VALUES (1004, N'bez mapy', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[client] OFF
GO
SET IDENTITY_INSERT [dbo].[client_service] ON 
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (3, N'1b4b7990-0c45-4b5c-a871-a9d2b8445ead', 1, 4, CAST(N'2020-07-13T11:49:18.3256299' AS DateTime2), NULL)
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (4, N'68698fca-4054-455d-b280-e5025faa005c', 1, 4, CAST(N'2020-07-13T11:59:58.7549877' AS DateTime2), NULL)
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (5, N'6a290a8b-9466-4c90-b406-01bed1c2c005', 1, 9, CAST(N'2020-07-13T12:07:00.3684192' AS DateTime2), NULL)
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (7, N'4f3af5f7-bda4-40bf-8382-8ec3c45bb2a7', 1, 6, CAST(N'2020-07-13T12:07:09.2515996' AS DateTime2), NULL)
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (9, N'f44709ca-be22-4190-8246-510e5a65b32a', 1, 1, CAST(N'2020-07-13T12:09:04.0701455' AS DateTime2), NULL)
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (10, N'e7f66006-d9ec-462c-b94f-1b90f4a7e2e0', 1, 1, CAST(N'2020-07-13T12:10:58.8974322' AS DateTime2), N'test')
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (11, N'6d969874-0ba4-49e8-aa20-37ca898d62e8', 1, 1, CAST(N'2020-07-15T11:53:16.5246011' AS DateTime2), N'bardzo dluga nazwa klienta + postman')
GO
INSERT [dbo].[client_service] ([client_service_id], [token], [client_id], [service_id], [start_date], [description]) VALUES (12, N'5fa1b3aa-70a6-4bad-be11-9beccfafa559', 3, 2, CAST(N'2020-07-15T11:57:06.9690345' AS DateTime2), NULL)
GO
SET IDENTITY_INSERT [dbo].[client_service] OFF
GO
SET IDENTITY_INSERT [dbo].[employee] ON 
GO
INSERT [dbo].[employee] ([employee_id], [first_name], [last_name], [phone], [email], [description]) VALUES (1, N'Adam', N'Solon', N'123123123', N'asd@asd.ad', N'test')
GO
INSERT [dbo].[employee] ([employee_id], [first_name], [last_name], [phone], [email], [description]) VALUES (2, N'drugi', N'pracownik', N'123123123', N'asd@ad.a', N'drugi robol')
GO
INSERT [dbo].[employee] ([employee_id], [first_name], [last_name], [phone], [email], [description]) VALUES (3, N'test', N'evemt', N'123123123', N'123123', N'123123')
GO
SET IDENTITY_INSERT [dbo].[employee] OFF
GO
INSERT [dbo].[employee_point] ([EmployeeId], [PointId]) VALUES (1, 5)
GO
INSERT [dbo].[EmployeeEvent] ([EployeeId], [EventId]) VALUES (1, 19)
GO
INSERT [dbo].[EmployeeEvent] ([EployeeId], [EventId]) VALUES (1, 1019)
GO
SET IDENTITY_INSERT [dbo].[event] ON 
GO
INSERT [dbo].[event] ([event_id], [description], [date], [PointId], [status], [description_long]) VALUES (19, N'testr viewmodel', CAST(N'2020-08-23T21:54:00.0000000' AS DateTime2), 5, 0, N'')
GO
INSERT [dbo].[event] ([event_id], [description], [date], [PointId], [status], [description_long]) VALUES (21, N'test view model bez pracownika', CAST(N'2020-08-15T21:58:00.0000000' AS DateTime2), 9, 0, N'')
GO
INSERT [dbo].[event] ([event_id], [description], [date], [PointId], [status], [description_long]) VALUES (22, N'test view model bez pracownika', CAST(N'2020-08-15T21:59:00.0000000' AS DateTime2), 8, 0, N'')
GO
INSERT [dbo].[event] ([event_id], [description], [date], [PointId], [status], [description_long]) VALUES (1019, N'test status', CAST(N'2020-08-23T09:44:00.0000000' AS DateTime2), 5, 3, N'')
GO
INSERT [dbo].[event] ([event_id], [description], [date], [PointId], [status], [description_long]) VALUES (1020, N'klient a cos tma chce', CAST(N'2020-08-15T10:16:00.0000000' AS DateTime2), 8, 0, N'')
GO
INSERT [dbo].[event] ([event_id], [description], [date], [PointId], [status], [description_long]) VALUES (1021, N'123123123', CAST(N'2020-08-28T10:22:00.0000000' AS DateTime2), 14, 1, N'')
GO
INSERT [dbo].[event] ([event_id], [description], [date], [PointId], [status], [description_long]) VALUES (1022, N'test dlugiego opisu', CAST(N'2020-08-28T12:39:00.0000000' AS DateTime2), 13, 2, N'to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`to jest dlugi opis zobaczmy czy dziala`')
GO
SET IDENTITY_INSERT [dbo].[event] OFF
GO
SET IDENTITY_INSERT [dbo].[point] ON 
GO
INSERT [dbo].[point] ([point_id], [name], [description], [latitude], [longitude], [client_id]) VALUES (5, N'p2', N'opis pi2', N'52.545486', N' 25.708008', 4)
GO
INSERT [dbo].[point] ([point_id], [name], [description], [latitude], [longitude], [client_id]) VALUES (8, N'test2', N'test2', N'53.103711', N' 22.126465', 1)
GO
INSERT [dbo].[point] ([point_id], [name], [description], [latitude], [longitude], [client_id]) VALUES (9, N'as', N'as', N'52.451751', N' 15.556641', 3)
GO
INSERT [dbo].[point] ([point_id], [name], [description], [latitude], [longitude], [client_id]) VALUES (11, N'asd', N'asd', N'52.895649', N' 20.76416', 4)
GO
INSERT [dbo].[point] ([point_id], [name], [description], [latitude], [longitude], [client_id]) VALUES (12, N'qwe', N'qwe', N'54.188155', N' 16.699219', 3)
GO
INSERT [dbo].[point] ([point_id], [name], [description], [latitude], [longitude], [client_id]) VALUES (13, N'test w klienecie', N'asd', N'53.167929', N' 22.08252', 1)
GO
INSERT [dbo].[point] ([point_id], [name], [description], [latitude], [longitude], [client_id]) VALUES (14, N'test p oraz kolejny', N'asd', N'50.29656', N' 19.555664', 1)
GO
SET IDENTITY_INSERT [dbo].[point] OFF
GO
SET IDENTITY_INSERT [dbo].[result] ON 
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (76, CAST(N'2020-07-21T13:51:26.2759624' AS DateTime2), 1, N'tutaj sa logi', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (77, CAST(N'2020-07-21T13:51:28.2870265' AS DateTime2), 1, N'tutaj sa logi', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (78, CAST(N'2020-07-21T13:51:29.0779073' AS DateTime2), 1, N'tutaj sa logi', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (79, CAST(N'2020-07-21T13:51:29.5600587' AS DateTime2), 1, N'tutaj sa logi', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (80, CAST(N'2020-07-21T13:51:30.0224920' AS DateTime2), 1, N'tutaj sa logi', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (81, CAST(N'2020-07-21T13:51:53.2977861' AS DateTime2), 1, N'blablabl', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (82, CAST(N'2020-07-21T13:51:53.6336768' AS DateTime2), 1, N'blablabl', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (83, CAST(N'2020-07-21T13:51:54.2346927' AS DateTime2), 1, N'blablabl', N'a tutaj errory', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (84, CAST(N'2020-07-21T13:52:00.9951976' AS DateTime2), 1, N'blablabl', N'ehgashaahh', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (85, CAST(N'2020-07-21T13:52:01.2928505' AS DateTime2), 1, N'blablabl', N'ehgashaahh', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (86, CAST(N'2020-07-21T13:52:18.7145385' AS DateTime2), 1, N'blablabl', N'tewtetetete', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (87, CAST(N'2020-07-21T13:52:19.3080368' AS DateTime2), 1, N'blablabl', N'tewtetetete', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (88, CAST(N'2020-07-21T13:52:19.7732969' AS DateTime2), 1, N'blablabl', N'tewtetetete', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (89, CAST(N'2020-07-21T13:52:20.1344829' AS DateTime2), 1, N'blablabl', N'tewtetetete', 3)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1076, CAST(N'2020-07-22T12:12:45.5723676' AS DateTime2), 0, N'test false', N'falsz test', 12)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1077, CAST(N'2020-07-22T12:12:46.7512019' AS DateTime2), 0, N'test false', N'falsz test', 12)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1078, CAST(N'2020-07-22T12:12:47.5677773' AS DateTime2), 0, N'test false', N'falsz test', 12)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1079, CAST(N'2020-07-22T12:13:17.3628813' AS DateTime2), 0, N'test false', N'falsz test', 12)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1080, CAST(N'2020-07-22T12:13:17.8729122' AS DateTime2), 0, N'test false', N'falsz test', 12)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1081, CAST(N'2020-07-22T12:13:18.3296598' AS DateTime2), 0, N'test false', N'falsz test', 12)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1082, CAST(N'2020-07-22T12:13:33.2762627' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1083, CAST(N'2020-07-22T12:13:33.9862752' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1084, CAST(N'2020-07-22T12:13:34.3934402' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1085, CAST(N'2020-07-22T12:13:34.7497962' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1086, CAST(N'2020-07-22T12:13:34.9614141' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1087, CAST(N'2020-07-22T12:13:35.1804300' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1088, CAST(N'2020-07-22T12:13:35.4320744' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1089, CAST(N'2020-07-22T12:13:35.6434265' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1090, CAST(N'2020-07-22T12:13:35.8688445' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1091, CAST(N'2020-07-22T12:13:36.0556897' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1092, CAST(N'2020-07-22T12:13:36.2727533' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1093, CAST(N'2020-07-22T12:16:13.4385031' AS DateTime2), NULL, N'', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1094, CAST(N'2020-07-22T12:16:15.9144032' AS DateTime2), NULL, N' ', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1095, CAST(N'2020-07-22T12:24:40.5115296' AS DateTime2), 1, N'', N'', 7)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1099, CAST(N'2020-07-22T13:56:43.3458149' AS DateTime2), NULL, N'test null', N'null test', 11)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1100, CAST(N'2020-07-24T12:49:45.6164628' AS DateTime2), 1, N'test', NULL, 4)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (1101, CAST(N'2020-07-24T13:23:25.5458931' AS DateTime2), 1, N'', N'', 7)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (2100, CAST(N'2020-07-27T11:29:16.9096517' AS DateTime2), 1, N'', N'', 7)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (2101, CAST(N'2020-07-27T11:29:24.5802608' AS DateTime2), 1, N'', N'using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace praktyki.Controllers
{
    [Route(api/[controller])]
    [ApiController]
    public class GuidController : ControllerBase
    {
        // GET: api/<GuidController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            yield return Guid.NewGuid().ToString();
        }', 7)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (2102, CAST(N'2020-07-27T11:47:58.6632269' AS DateTime2), 1, N'', N'using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace praktyki.Controllers
{
    [Route(api/[controller])]
    [ApiController]
    public class GuidController : ControllerBase
    {
        // GET: api/<GuidController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            yield return Guid.NewGuid().ToString();
        }', 7)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (3100, CAST(N'2020-07-29T11:19:26.8465027' AS DateTime2), 1, N'', N'', 7)
GO
INSERT [dbo].[result] ([result_id], [datetime], [status], [logs], [errors], [client_service_id]) VALUES (3101, CAST(N'2020-08-26T14:58:45.7869656' AS DateTime2), 1, N'', N'', 7)
GO
SET IDENTITY_INSERT [dbo].[result] OFF
GO
SET IDENTITY_INSERT [dbo].[service] ON 
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (1, N'test', N'desctest')
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (2, N'postman', N'test request')
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (3, N'asd', N'asd')
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (4, N'test', N'bla')
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (5, N'test', N'bla')
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (6, N'123', N'123')
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (7, N'456', N'456')
GO
INSERT [dbo].[service] ([service_id], [name], [description]) VALUES (9, N'vbn', N'vbn')
GO
SET IDENTITY_INSERT [dbo].[service] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 30.08.2020 17:00:13 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 30.08.2020 17:00:13 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_EmployeePoint_PointId]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_EmployeePoint_PointId] ON [dbo].[employee_point]
(
	[PointId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_EmployeeEvent_EventId]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_EmployeeEvent_EventId] ON [dbo].[EmployeeEvent]
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_event_PointId]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_event_PointId] ON [dbo].[event]
(
	[PointId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_point_client_id]    Script Date: 30.08.2020 17:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_point_client_id] ON [dbo].[point]
(
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[client_service] ADD  CONSTRAINT [DF_client_service_token]  DEFAULT (newid()) FOR [token]
GO
ALTER TABLE [dbo].[client_service] ADD  CONSTRAINT [DF_client_service_start_date]  DEFAULT (getdate()) FOR [start_date]
GO
ALTER TABLE [dbo].[event] ADD  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[event] ADD  DEFAULT (N'') FOR [description_long]
GO
ALTER TABLE [dbo].[result] ADD  DEFAULT (getdate()) FOR [datetime]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[client_service]  WITH CHECK ADD  CONSTRAINT [FK_client_service_client] FOREIGN KEY([client_id])
REFERENCES [dbo].[client] ([client_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[client_service] CHECK CONSTRAINT [FK_client_service_client]
GO
ALTER TABLE [dbo].[client_service]  WITH CHECK ADD  CONSTRAINT [FK_client_service_service] FOREIGN KEY([service_id])
REFERENCES [dbo].[service] ([service_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[client_service] CHECK CONSTRAINT [FK_client_service_service]
GO
ALTER TABLE [dbo].[employee_point]  WITH CHECK ADD  CONSTRAINT [FK_EmployeePoint_employee_EmployeeId] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[employee] ([employee_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[employee_point] CHECK CONSTRAINT [FK_EmployeePoint_employee_EmployeeId]
GO
ALTER TABLE [dbo].[employee_point]  WITH CHECK ADD  CONSTRAINT [FK_EmployeePoint_point_PointId] FOREIGN KEY([PointId])
REFERENCES [dbo].[point] ([point_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[employee_point] CHECK CONSTRAINT [FK_EmployeePoint_point_PointId]
GO
ALTER TABLE [dbo].[EmployeeEvent]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeEvent_employee_EployeeId] FOREIGN KEY([EployeeId])
REFERENCES [dbo].[employee] ([employee_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EmployeeEvent] CHECK CONSTRAINT [FK_EmployeeEvent_employee_EployeeId]
GO
ALTER TABLE [dbo].[EmployeeEvent]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeEvent_event_EventId] FOREIGN KEY([EventId])
REFERENCES [dbo].[event] ([event_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EmployeeEvent] CHECK CONSTRAINT [FK_EmployeeEvent_event_EventId]
GO
ALTER TABLE [dbo].[event]  WITH CHECK ADD  CONSTRAINT [FK_event_point_PointId] FOREIGN KEY([PointId])
REFERENCES [dbo].[point] ([point_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[event] CHECK CONSTRAINT [FK_event_point_PointId]
GO
ALTER TABLE [dbo].[point]  WITH CHECK ADD  CONSTRAINT [FK_point_client_client_id] FOREIGN KEY([client_id])
REFERENCES [dbo].[client] ([client_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[point] CHECK CONSTRAINT [FK_point_client_client_id]
GO
ALTER TABLE [dbo].[result]  WITH CHECK ADD  CONSTRAINT [FK_result_service] FOREIGN KEY([client_service_id])
REFERENCES [dbo].[client_service] ([client_service_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[result] CHECK CONSTRAINT [FK_result_service]
GO
USE [master]
GO
ALTER DATABASE [PRAKTYKI] SET  READ_WRITE 
GO
